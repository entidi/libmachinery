#ifndef MACHINERY_PUSH_BUTTON_H_
#define MACHINERY_PUSH_BUTTON_H_

#include <gtk/gtk.h>


G_BEGIN_DECLS

#define MACHINERY_TYPE_PUSH_BUTTON  machinery_push_button_get_type()

G_DECLARE_FINAL_TYPE(MachineryPushButton, machinery_push_button, MACHINERY, PUSH_BUTTON, GtkCheckButton)

GtkWidget *     machinery_push_button_new           (void);
GtkWidget *     machinery_push_button_new_with_label(const gchar *  label);

G_END_DECLS

#endif /* MACHINERY_PUSH_BUTTON_H_ */
