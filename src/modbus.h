#ifndef MACHINERY_MODBUS_H_
#define MACHINERY_MODBUS_H_

#include "gobject.h"

#define MACHINERY_TYPE_MODBUS             (machinery_modbus_get_type())
#define MACHINERY_MODBUS(obj)             (G_TYPE_CHECK_INSTANCE_CAST((obj), MACHINERY_TYPE_MODBUS, MachineryModbus))
#define MACHINERY_MODBUS_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST((klass), MACHINERY_TYPE_MODBUS, MachineryModbusClass))
#define MACHINERY_IS_MODBUS(obj)          (G_TYPE_CHECK_INSTANCE_TYPE((obj), MACHINERY_TYPE_MODBUS))
#define MACHINERY_IS_MODBUS_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE((klass), MACHINERY_TYPE_MODBUS))
#define MACHINERY_MODBUS_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS((obj), MACHINERY_TYPE_MODBUS, MachineryModbusClass))


G_BEGIN_DECLS

typedef struct _MachineryModbus        MachineryModbus;
typedef struct _MachineryModbusClass   MachineryModbusClass;

struct _MachineryModbus {
    GObject         parent;
};

struct _MachineryModbusClass {
    GObjectClass    parent_class;
};


MachineryModbus*machinery_modbus_new            (void);
MachineryModbus*machinery_modbus_new_full       (const gchar *      devnet,
                                                 guint              baudport);
gpointer        machinery_modbus_get_handle     (MachineryModbus *  modbus);
void            machinery_modbus_set_devnet     (MachineryModbus *  modbus,
                                                 const gchar *      devnet);
const gchar *   machinery_modbus_get_devnet     (MachineryModbus *  modbus);
void            machinery_modbus_set_baudport   (MachineryModbus *  modbus,
                                                 guint              baudport);
guint           machinery_modbus_get_baudport   (MachineryModbus *  modbus);
void            machinery_modbus_set_slave_id   (MachineryModbus *  modbus,
                                                 guint              slave_id);
guint           machinery_modbus_get_slave_id   (MachineryModbus *  modbus);
void            machinery_modbus_set_rate       (MachineryModbus *  modbus,
                                                 guint              rate);
guint           machinery_modbus_get_rate       (MachineryModbus *  modbus);
void            machinery_modbus_set_callback   (MachineryModbus *  modbus,
                                                 GSourceFunc        callback);
gboolean        machinery_modbus_start          (MachineryModbus *  modbus);
void            machinery_modbus_stop           (MachineryModbus *  modbus);
gboolean        machinery_modbus_is_running     (MachineryModbus *  modbus);
gchar *         machinery_modbus_get_error      (MachineryModbus *  modbus);

G_END_DECLS

#endif /* MACHINERY_MODBUS_H_ */
