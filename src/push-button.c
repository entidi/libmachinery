/**
 * @file  machinery-push-button.c
 * @brief A GtkCheckButton that behaves like a GtkButton.
 */

#include "push-button.h"
#include <glib-object.h>

struct _MachineryPushButton {
    GtkCheckButton check_button;
};

G_DEFINE_TYPE(MachineryPushButton, machinery_push_button, GTK_TYPE_CHECK_BUTTON)


static void
machinery_push_button_clicked(GtkButton *button)
{
    /* Directly use GtkButtonClass instead of the parent class to skip
     * whatever done by GtkToggleButton to retain its toggled state */
    GtkButtonClass *button_class = g_type_class_peek(GTK_TYPE_BUTTON);
    if (button_class->clicked != NULL) {
        button_class->clicked(button);
    }
}

static void
button_set_active(GtkButton *button, gboolean state)
{
    GtkToggleButton *toggle_button = GTK_TOGGLE_BUTTON(button);
    gboolean old_state = gtk_toggle_button_get_active(toggle_button);
    if (state != old_state) {
        /* Use the "clicked" event of the parent class to switch
         * the active state */
        GtkButtonClass *parent_class = GTK_BUTTON_CLASS(machinery_push_button_parent_class);
        if (parent_class->clicked != NULL) {
            parent_class->clicked(button);
        }
    }
}

static void
machinery_push_button_pressed(GtkButton *button)
{
    GtkButtonClass *parent_class = GTK_BUTTON_CLASS(machinery_push_button_parent_class);
    button_set_active(button, TRUE);
    if (parent_class->pressed != NULL) {
        parent_class->pressed(button);
    }
}

static void
machinery_push_button_released(GtkButton *button)
{
    GtkButtonClass *parent_class = GTK_BUTTON_CLASS(machinery_push_button_parent_class);
    button_set_active(button, FALSE);
    if (parent_class->released != NULL) {
        parent_class->released(button);
    }
}

static void
machinery_push_button_class_init(MachineryPushButtonClass *class)
{
    GtkButtonClass *button_class = (GtkButtonClass*) class;
    button_class->clicked = machinery_push_button_clicked;
    button_class->pressed = machinery_push_button_pressed;
    button_class->released = machinery_push_button_released;
}

static void
machinery_push_button_init(MachineryPushButton *push_button)
{
}

GtkWidget *
machinery_push_button_new(void)
{
    return g_object_new(MACHINERY_TYPE_PUSH_BUTTON, NULL);
}

GtkWidget *
machinery_push_button_new_with_label(const gchar *label)
{
    return g_object_new(MACHINERY_TYPE_PUSH_BUTTON, "label", label, NULL);
}
