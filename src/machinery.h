/**
 * @file  machinery.h
 * @brief General purpose functions.
 */

#ifndef MACHINERY_H_
#define MACHINERY_H_

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>

#define PDO1    0
#define PDO2    1
#define PDO3    2
#define PDO4    3
#define NULLMAP ((uint32_t) 0)


typedef struct {
    char *      name;       /**< Name of the project */
    char *      module;     /**< Full name of the module, if relevant */
    char *      pid;        /**< Path to the pid file, if relevant */
    void *      share;      /**< Shared memory data */
    void *      lua;        /**< Possible Lua state */
    char *      lua_name;   /**< Library name specific to Lua */
    void *      canopen;    /**< Possible CANopen data */
    void *      ethercat;   /**< Possible EtherCAT data */
    void *      timer;      /**< Possible periodic timer data */
    void *      influencer; /**< Possible influencer data */
} App;

typedef enum {
    APP_SUCCESS = 0,    /**< The application has been successfully executed */
    APP_FAILURE = 1,    /**< Generic failure during the application initialization */
    APP_INVALID = 255,  /**< Invalid return value: should never happen */
} AppStatus;

typedef enum {
    ACTION_WAITING  = 0,/**< Waiting, typically for an I/O response */
    ACTION_DONE     = 1,/**< Action terminated successfully */
    ACTION_ERROR    = 2,/**< An error occurred */
    ACTION_COMPLETE = 3,/**< Used in a multistep action to signal that
                             all steps have terminated successfully  */
} ActionStatus;

typedef ActionStatus (*Configure)(App *app, uint8_t slave_id, int step);
typedef void (*ODCallback)(App *app, uint32_t map, const void *value);
typedef void (*QuitHandler)(AppStatus status);


#ifdef __cplusplus
extern "C" {
#endif

/* Basic helper functions */
char *          malloc_strcat               (const char *   str1,
                                             const char *   str2);
char *          malloc_printf               (const char *   format,
                                             ...);
uint32_t        get_hash                    (const char *   text);

void            debug                       (const char *   format,
                                             ...);
void            info                        (const char *   format,
                                             ...);
void            warning                     (const char *   format,
                                             ...);
void            die                         (const char *   format,
                                             ...);
void            die_with_errno              (const char *   caller);
void            quit                        (AppStatus      status);
void            quit_set_handler            (QuitHandler    handler);

/* Application support is always enabled */
void            app_init                    (App *          app,
                                             const char *   name);
void            app_free                    (App *          app);

/* Sharing support is always enabled */
void            app_share_init              (App *          app,
                                             int            n_zones,
                                             ...);
void            app_share_free              (App *          app);
size_t          app_share_size              (App *          app);
bool            app_share_watch             (App *          app,
                                             int            n_zone);
bool            app_share_read              (App *          app,
                                             int            n_zone,
                                             void *         dst,
                                             const void *   mask,
                                             long           timeout);
bool            app_share_write             (App *          app,
                                             int            n_zone,
                                             const void *   src,
                                             const void *   mask,
                                             long           timeout);

/* Available only if libdaemon support has been enabled */
void            app_daemon_init             (App *          app,
                                             const char *   module);
void            app_daemon_free             (App *          app);
void            app_daemon_kill             (App *          app);
int             app_daemon_start            (App *          app);

/* Lua support is always enabled */
void            app_lua_set_name            (App *          app,
                                             const char *   name);
void            app_lua_init                (App *          app,
                                             const char *   script,
                                             const void *   library);
void            app_lua_free                (App *          app);
void            app_lua_run                 (App *          app);

/* Available only if CANopen support has been enabled */
void            app_canopen_init            (App *          app,
                                             const char *   device,
                                             int            baudrate);
void            app_canopen_free            (App *          app);
void            app_canopen_set_id          (App *          app,
                                             uint8_t        master_id);
void            app_canopen_set_data        (App *          app,
                                             void *         data);
void            app_canopen_set_transmitter (App *          app,
                                             ODCallback     transmitter);
void            app_canopen_set_receiver    (App *          app,
                                             ODCallback     receiver);
void            app_canopen_add_slave       (App *          app,
                                             uint8_t        slave_id,
                                             Configure      configure);
int             app_canopen_autoconf_slaves (App *          app);
void            app_canopen_start           (App *          app);
void            app_canopen_stop            (App *          app);
void            app_canopen_reset           (App *          app,
                                             uint8_t        slave_id);
void            app_canopen_settings        (App *          app,
                                             uint8_t        slave_id,
                                             int32_t        inhibit,
                                             int32_t        event,
                                             int            max_scans,
                                             int            max_errors,
                                             int            is_blind);
ActionStatus    app_canopen_map_tpdoa       (App *          app,
                                             uint8_t        slave_id,
                                             uint8_t        pdo,
                                             uint8_t        n_maps,
                                             uint32_t       maps[]);
ActionStatus    app_canopen_map_tpdos       (App *          app,
                                             uint8_t        slave_id,
                                             uint8_t        pdo,
                                             int            n,
                                             ...);
ActionStatus    app_canopen_map_tpdo        (App *          app,
                                             uint8_t        slave_id,
                                             uint8_t        pdo,
                                             uint32_t       map);
ActionStatus    app_canopen_map_rpdoa       (App *          app,
                                             uint8_t        slave_id,
                                             uint8_t        pdo,
                                             uint8_t        n_maps,
                                             uint32_t       maps[]);
ActionStatus    app_canopen_map_rpdos       (App *          app,
                                             uint8_t        slave_id,
                                             uint8_t        pdo,
                                             int            n,
                                             ...);
ActionStatus    app_canopen_map_rpdo        (App *          app,
                                             uint8_t        slave_id,
                                             uint8_t        pdo,
                                             uint32_t       map);
ActionStatus    app_canopen_read_sdo        (App *          app,
                                             uint8_t        slave_id,
                                             uint32_t       map,
                                             void *         value,
                                             int            timeout);
ActionStatus    app_canopen_write_sdo       (App *          app,
                                             uint8_t        slave_id,
                                             uint32_t       map,
                                             uint32_t       value,
                                             int            timeout);

/* Available only if EtherCAT support has been enabled */
ActionStatus    app_ethercat_init           (App *          app);
void            app_ethercat_free           (App *          app);
void *          app_ethercat_get_master     (App *          app);
ActionStatus    app_ethercat_add_slave      (App *          app,
                                             uint16_t       alias,
                                             uint16_t       position,
                                             const void *   configuration);
ActionStatus    app_ethercat_autoadd_slaves (App *          app);
ActionStatus    app_ethercat_configure_slave(App *          app,
                                             unsigned       nslave,
                                             ...);
ActionStatus    app_ethercat_set_watchdog   (App *          app,
                                             unsigned       nslave,
                                             uint16_t       divider,
                                             uint16_t       intervals);
ActionStatus    app_ethercat_start          (App *          app);
ActionStatus    app_ethercat_start_async    (App *          app);
ActionStatus    app_ethercat_online         (App *          app);
void            app_ethercat_stop           (App *          app);
void *          app_ethercat_image          (App *          app,
                                             unsigned       nslave,
                                             unsigned       nsync);
void            app_ethercat_send           (App *          app);
ActionStatus    app_ethercat_receive        (App *          app);
ActionStatus    app_ethercat_sdo_timeout    (App *          app,
                                             unsigned       nslave,
                                             uint32_t       ms);
ActionStatus    app_ethercat_sdo_upload     (App *          app,
                                             unsigned       nslave,
                                             uint32_t       map,
                                             void *         value);
ActionStatus    app_ethercat_sdo_download   (App *          app,
                                             unsigned       nslave,
                                             uint32_t       map,
                                             uint32_t       value);

/* Timing support is always enabled */
void            app_timer_init              (App *          app,
                                             long           usec);
void            app_timer_free              (App *          app);
void            app_timer_wait              (App *          app);
long            app_timer_get_missed        (App *          app);
double          app_timer_get_busy          (App *          app);
void            app_timer_clear_busy        (App *          app);

/* Influencer is always enabled */
void            app_influencer_init         (App *          app);
void            app_influencer_free         (App *          app);
void            app_influencer_push         (App *          app,
                                             const char *   action,
                                             const char *   channel,
                                             double         data);
void            app_influencer_pop          (App *          app,
                                             const char **  action,
                                             const char **  channel,
                                             double *       data);
int             app_influencer_queued       (App *          app);

#ifdef __cplusplus
}
#endif

#endif /* MACHINERY_H_ */
