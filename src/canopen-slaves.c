/**
 * @file  canopen-slaves.c
 * @brief Configurations for common CANopen slaves.
 *
 * This is a collection of configuration callbacks for some common CANopen
 * device. Anyone of them can be passed as `ConfigureCallback` argument to the
 * app_canopen_add_slave() function.
 *
 * All functions are idempotents and reentrants, i.e. they can be called
 * multiple times and from different contexts.
 */

#include "machinery.h"
#include "slaves.h"


/**
 * Configuration for an Advantys node with DDI3725 + DDO3605 or equivalent.
 *
 * The 16 digital inputs will be mapped to TxPDO1 while the 8 digital outputs
 * will be mapped to RxPDO1.
 *
 * @param app      The main App instance
 * @param slave_id CANbus ID of the node to configure
 * @param step     Caller provided configuration step
 * @return         The current configuration status
 */
ActionStatus
dsp401_16di8do(App *app, uint8_t slave_id, int step)
{
    switch (step) {

    case 0: /* TxPDO1 will map the 16 digital inputs */
        return app_canopen_map_tpdo(app, slave_id, PDO1, 0x61000110);

    case 1: /* RxPDO1 will map the 8 digital outputs */
        return app_canopen_map_rpdo(app, slave_id, PDO1, 0x62000108);

    default:
        return ACTION_COMPLETE;
    }
}

/**
 * Configuration for an Advantys node with DDI3725 + DDO3705 or equivalent.
 *
 * The 16 digital inputs will be mapped to TxPDO1 while the 16 digital outputs
 * will be mapped to RxPDO1.
 *
 * @param app      The main App instance
 * @param slave_id CANbus ID of the node to configure
 * @param step     Caller provided configuration step
 * @return         The current configuration status
 */
ActionStatus
dsp401_16di16do(App *app, uint8_t slave_id, int step)
{
    switch (step) {

    case 0: /* TxPDO1 will map the 16 digital inputs */
        return app_canopen_map_tpdo(app, slave_id, PDO1, 0x61000110);

    case 1: /* RxPDO1 will map the 16 digital outputs */
        return app_canopen_map_rpdo(app, slave_id, PDO1, 0x63000110);
        break;

    default:
        return ACTION_COMPLETE;
    }
}

/**
 * Configuration for an Advantys node with DDI3725 + DDO3605 + DDO3605 or
 * equivalent.
 *
 * The 16 digital inputs will be mapped to TxPDO1 while the 6+6 digital outputs
 * will be respectively mapped to two different bytes of RxPDO1.
 *
 * @param app      The main App instance
 * @param slave_id CANbus ID of the node to configure
 * @param step     Caller provided configuration step
 * @return         The current configuration status
 */
ActionStatus
dsp401_16di8do8do(App *app, uint8_t slave_id, int step)
{
    switch (step) {

    case 0: /* TxPDO1 will map the 16 digital inputs */
        return app_canopen_map_tpdo(app, slave_id, PDO1, 0x61000110);

    case 1: /* RxPDO1 will map the 6+6 (handled as 8+8) digital outputs */
        return app_canopen_map_rpdos(app, slave_id, PDO1, 2, 0x62000108, 0x62000208);

    default:
        return ACTION_COMPLETE;
    }
}

/**
 * Configuration for a generic DSP402 motor in profile position mode.
 *
 * The status word and 8 digital inputs will be mapped to TxPDO1, the current
 * position (INT32) to TxPDO2, the control word to RxPDO1 and the destination
 * position (INT32) to RxPDO2.
 *
 * @param app      The main App instance
 * @param slave_id CANbus ID of the node to configure
 * @param step     Caller provided configuration step
 * @return         The current configuration status
 */
ActionStatus
dsp402_profile_position(App *app, uint8_t slave_id, int step)
{
    ActionStatus result;

    switch (step) {

    case 0: /* TxPDO1 will map the status word and 8 digital inputs*/
        return app_canopen_map_tpdos(app, slave_id, PDO1, 2, 0x60410010, 0x60FD0008);

    case 1: /* RxPDO1 will map the control word */
        return app_canopen_map_rpdo(app, slave_id, PDO1, 0x60400010);

    case 2: /* TxPDO2 will map the current position */
        app_canopen_settings(app, slave_id, 5000, -2, -2, -2, -2);
        result = app_canopen_map_tpdo(app, slave_id, PDO2, 0x60640020);
        app_canopen_settings(app, slave_id, 0, -2, -2, -2, -2);
        return result;

    case 3: /* RxPDO2 will map the destination position */
        return app_canopen_map_rpdo(app, slave_id, PDO2, 0x607A0020);

    default:
        return ACTION_COMPLETE;
    }
}

/**
 * Configuration for a generic DSP402 motor in profile velocity mode.
 *
 * The status word will be mapped to TxPDO1, the velocity actual value (INT32)
 * to TxPDO2, the control word to RxPDO1 and the target velocity (INT32) to
 * RxPDO2.
 *
 * @param app      The main App instance
 * @param slave_id CANbus ID of the node to configure
 * @param step     Caller provided configuration step
 * @return         The current configuration status
 */
ActionStatus
dsp402_profile_velocity(App *app, uint8_t slave_id, int step)
{
    ActionStatus result;

    switch (step) {

    case 0: /* TxPDO1 will map the status word */
        return app_canopen_map_tpdo(app, slave_id, PDO1, 0x60410010);

    case 1: /* RxPDO1 will map the control word */
        return app_canopen_map_rpdo(app, slave_id, PDO1, 0x60400010);

    case 2: /* TxPDO2 will map the velocity actual value */
        app_canopen_settings(app, slave_id, 5000, -2, -2, -2, -2);
        result = app_canopen_map_tpdo(app, slave_id, PDO2, 0x606C0020);
        app_canopen_settings(app, slave_id, 0, -1, -2, -2, -2);
        return result;

    case 3: /* RxPDO2 will map the target velocity */
        return app_canopen_map_rpdo(app, slave_id, PDO2, 0x60FF0020);

    default:
        return ACTION_COMPLETE;
    }
}

/**
 * Configuration for a generic DSP402 motor in velocity mode.
 *
 * The status word will be mapped to TxPDO1, the velocity actual value (INT16)
 * to TxPDO2, the control word to RxPDO1 and the target velocity (INT16) to
 * RxPDO2.
 *
 * @param app      The main App instance
 * @param slave_id CANbus ID of the node to configure
 * @param step     Caller provided configuration step
 * @return         The current configuration status
 */
ActionStatus
dsp402_velocity(App *app, uint8_t slave_id, int step)
{
    ActionStatus result;

    switch (step) {

    case 0: /* TxPDO1 will map the status word */
        return app_canopen_map_tpdo(app, slave_id, PDO1, 0x60410010);

    case 1: /* RxPDO1 will map the control word */
        return app_canopen_map_rpdo(app, slave_id, PDO1, 0x60400010);

    case 2: /* TxPDO2 will map the velocity actual value */
        app_canopen_settings(app, slave_id, 5000, -2, -2, -2, -2);
        result = app_canopen_map_tpdo(app, slave_id, PDO2, 0x60440010);
        app_canopen_settings(app, slave_id, 0, -2, -2, -2, -2);
        return result;

    case 3: /* RxPDO2 will map the target velocity */
        return app_canopen_map_rpdo(app, slave_id, PDO2, 0x60420010);

    default:
        return ACTION_COMPLETE;
    }
}
