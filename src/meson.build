libmachinery_c = files([
    'machinery.c',
    'quit.c',
    'log.c',
    'app.c',
    'share.c',
    'timer.c',
    'ramp.c',
    'influencer.c',
])

libmachinery_h_public = files([
    'machinery.h',
    'gobject.h',
    'ramp.h',
])

libmachinery_h_private = files([
    'share-private.h',
])

libmachinery_deps = [
    libm,
    librt,
    thread_dep,
    gobject_dep,
]

if daemon_dep.found()
    libmachinery_c += files([
        'daemon.c',
    ])
    libmachinery_deps += [
        daemon_dep,
    ]
endif

if serialport_dep.found()
    libmachinery_c += files([
        'grbl.c',
    ])
    libmachinery_h_public += files([
        'grbl.h',
    ])
    libmachinery_deps += [
        serialport_dep,
    ]
endif

if modbus_dep.found()
    libmachinery_c += files([
        'modbus.c',
    ])
    libmachinery_h_public += files([
        'modbus.h',
    ])
    libmachinery_deps += [
        modbus_dep,
    ]
endif

if canfestival_dep.found()
    libmachinery_c += files([
        'canopen.c',
        'canopen-slaves.c',
    ])
    # `mv slaves.h canopen-slaves.h` would break backward compatibility
    libmachinery_h_public += files([
        'slaves.h',
    ])
    libmachinery_deps += [
        canfestival_dep,
        # Workaround: canfestival_dep does not include all required libs
        cc.find_library('canfestival_unix'),
    ]
endif

if ethercat_dep.found()
    libmachinery_c += files([
        'ethercat.c',
        'ethercat-slaves.c',
    ])
    libmachinery_h_public += files([
        'ethercat-slaves.h',
        'ethercat-internal.h',
    ])
    libmachinery_deps += [
        ethercat_dep,
    ]
endif

if gtk3_dep.found()
    libmachinery_c += files([
        'gtk.c',
        'bindings.c',
        'deferred.c',
        'input.c',
        'permission.c',
        'push-button.c',
    ])
    libmachinery_h_public += files([
        'gtk.h',
        'push-button.h',
    ])
    libmachinery_deps += [
        gtk3_dep,
    ]
    # Install glade catalog, if possible
    gladeui_dep = dependency('gladeui-2.0', required: false)
    if gladeui_dep.found() and meson.version().version_compare('>=0.51')
        catalogdir = gladeui_dep.get_variable(pkgconfig: 'catalogdir')
        install_data('machinery.xml', install_dir: catalogdir)
    endif
endif

if lua_dep.found()
    libmachinery_c += files([
        'lua.c',
    ])
    libmachinery_deps += [
        lua_dep,
    ]
endif

libmachinery_sources = libmachinery_c + libmachinery_h_public + libmachinery_h_private

# `pkgdatadir` needs to be added to the Lua path
libmachinery_cflags = [
    '-DPKGDATADIR="' + pkgdatadir + '"',
]


# Install public headers
install_headers(libmachinery_h_public, subdir: 'libmachinery/')


# Main shared and static library, always built because wrapping
# functions in tests requires a static library
libmachinery = both_libraries('machinery',
                              soversion: libmachinery_metadata['soversion'],
                              sources: libmachinery_sources,
                              dependencies: libmachinery_deps,
                              c_args: libmachinery_cflags,
                              install: true)

# Define the dependency to be used by tests
libmachinery_dep = declare_dependency(
    include_directories: include_directories('.'),
    link_with: libmachinery.get_static_lib()
)

# libmachinery.pc handling
pkg = import('pkgconfig')
pkg.generate(libmachinery.get_shared_lib(),
             # All dependencies explicit for backward compatibility
             libraries: libmachinery_deps,
             name: 'libmachinery',
             description: libmachinery_metadata['description'],
             url: libmachinery_metadata['url'],
             variables: [
                 'pkgdatadir=' + join_paths('${prefix}', get_option('datadir'), meson.project_name()),
             ])

# Install data files
install_data(files(['dsp402.lua']),
             install_dir: pkgdatadir)

# Utility programs
hash = executable('machinery-hash',
                  files(['machinery.h', 'hash.c']),
                  link_with: libmachinery.get_shared_lib(),
                  install: true)

inspect = executable('machinery-inspect',
                     files(['machinery.h', 'share-private.h', 'inspect.c']),
                     link_with: libmachinery.get_shared_lib(),
                     install: true)
if canfestival_dep.found()
    cofe = executable('machinery-cofe',
                      files(['machinery.h', 'od/Dummy.h', 'od/Dummy.c', 'cofe.c']),
                      link_with: libmachinery.get_shared_lib(),
                      dependencies: [canfestival_dep],
                      install: true)
endif
