General information
===================

`libmachinery` is a (not so) general purpose library for speeding up the
developmenent of machines for industrial automation. It provides:

* a basic logging system
* memory sharing between processes
* extended scripting support via the Lua language
* a master CANopen stack
* a master EtherCAT stack
* a master Modbus stack
* automation-oriented helper functions for GTK based GUIs
* many more facilities

EtherCAT
--------

This stack is based on the [IgH master stack](https://etherlab.org/en/ethercat/).
The project is usually configured with the following command:

```
./configure \
    --sysconfdir=/etc \
    --enable-generic \
    --disable-8139too \
    --disable-initd
```

CANOpen
-------

This stack is based on [canfestival](https://canfestival.org). The
snapshot used is commit 84d7f6b2adcc5a60e0ba062fff412f75addb36a7 of
[this repository](https://github.com/fbsder/canfestival). It has been
configured with the following command:

```
# ArchLinux
./configure \
    --prefix=/usr/local \
    --python=python2 \
    --target=unix \
    --can=socket \
    --wx-config=wx-config-2.8 \
    --SDO_MAX_SIMULTANEOUS_TRANSFERS=32

# Ubuntu
./configure \
    --prefix=/usr/local \
    --python=python2 \
    --target=unix \
    --can=socket \
    --SDO_MAX_SIMULTANEOUS_TRANSFERS=32
```
