#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <machinery.h>
#include <ecrt.h>


void
__wrap_die(const char *format, ...)
{
    check_expected(format);
}

void
__wrap_die_with_errno(const char *caller)
{
    check_expected(caller);
}

void
__wrap_info(const char *format, ...)
{
    check_expected(format);
}

void
__wrap_warning(const char *format, ...)
{
    check_expected(format);
}

ec_master_t *
__wrap_ecrt_request_master(unsigned index)
{
    check_expected(index);
    return mock_type(ec_master_t *);
}

void
__wrap_ecrt_release_master(ec_master_t *master)
{
    check_expected(master);
}

ec_domain_t *
__wrap_ecrt_master_create_domain(ec_master_t *master)
{
    check_expected(master);
    return mock_type(ec_domain_t *);
}


static int
setup(void **state)
{
    App *app = malloc(sizeof(App));
    app_init(app, "test");
    *state = app;
    return 0;
}

static int
teardown(void **state)
{
    App *app = *state;
    app_free(app);
    free(app);
    return 0;
}

static void
test_ethercat_init_free(void **state)
{
    App *app = *state;
    void * old_ethercat;

    assert_null(app->ethercat);

    /* Test failing constructions */
    expect_value(__wrap_ecrt_request_master, index, 0);
    will_return(__wrap_ecrt_request_master, NULL);
    expect_string(__wrap_warning, format, "ecrt_request_master() failed");
    app_ethercat_init(app);
    assert_non_null(app->ethercat);
    app_ethercat_free(app);
    assert_null(app->ethercat);

    expect_value(__wrap_ecrt_request_master, index, 0);
    will_return(__wrap_ecrt_request_master, state);
    expect_value(__wrap_ecrt_master_create_domain, master, state);
    will_return(__wrap_ecrt_master_create_domain, NULL);
    expect_string(__wrap_warning, format, "ecrt_master_create_domain() failed");
    app_ethercat_init(app);
    assert_non_null(app->ethercat);

    old_ethercat = app->ethercat;

    /* Test successful construction */
    expect_value(__wrap_ecrt_request_master, index, 0);
    will_return(__wrap_ecrt_request_master, state);
    expect_value(__wrap_ecrt_master_create_domain, master, state);
    will_return(__wrap_ecrt_master_create_domain, state);
    app_ethercat_init(app);
    assert_non_null(app->ethercat);

    /* Check that the double initialization did not leak */
    assert_ptr_equal(app->ethercat, old_ethercat);

    /* Test successful destruction */
    expect_value(__wrap_ecrt_release_master, master, state);
    app_ethercat_free(app);
    assert_null(app->ethercat);

    /* Check further destructions result in a no-op */
    app_ethercat_free(app);
    assert_null(app->ethercat);
    app_ethercat_free(app);
    app_ethercat_free(app);
    assert_null(app->ethercat);
}

int
main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_ethercat_init_free),
    };
    return cmocka_run_group_tests(tests, setup, teardown);
}
