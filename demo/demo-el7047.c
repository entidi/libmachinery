#include "machinery.h"
#include "ethercat-slaves.h"
#include <lualib.h>
#include <lauxlib.h>


typedef struct __attribute__((packed)) {
    unsigned    stm_enable : 1;
    unsigned    stm_reset : 1;
    unsigned    stm_reduce_torque : 1;
    unsigned    : 8;
    unsigned    stm_do1 : 1;
    unsigned    : 4;
    unsigned    pos_execute : 1;
    unsigned    pos_emergency_stop : 1;
    unsigned    : 14;
    int         pos_target : 32;
    unsigned    pos_velocity : 16;
    unsigned    pos_start_type : 16;
    unsigned    pos_acceleration : 16;
    unsigned    pos_deceleration : 16;
} RxPdo_1;

typedef struct __attribute__((packed)) {
    unsigned    stm_ready_to_enable : 1;
    unsigned    stm_ready : 1;
    unsigned    stm_warning : 1;
    unsigned    stm_error : 1;
    unsigned    stm_forward : 1;
    unsigned    stm_backward : 1;
    unsigned    stm_torque_reduced : 1;
    unsigned    stm_stall : 1;
    unsigned    : 3;
    unsigned    stm_di1 : 1;
    unsigned    stm_di2 : 1;
    unsigned    stm_sync_error : 1;
    unsigned    : 1;
    unsigned    stm_pdo_toggle : 1;
    unsigned    pos_busy : 1;
    unsigned    pos_in_target : 1;
    unsigned    pos_warning : 1;
    unsigned    pos_error : 1;
    unsigned    pos_calibrated : 1;
    unsigned    pos_accelerate : 1;
    unsigned    pos_decelerate : 1;
    unsigned    pos_ready_to_execute : 1;
    unsigned    : 8;
    unsigned    pos_actual_position : 32;
    unsigned    pos_actual_velocity : 16;
    unsigned    pos_drive_time : 32;
} TxPdo_1;


static App app;
static RxPdo_1 *rxpdo = NULL;
static TxPdo_1 *txpdo = NULL;


static int
iteration(void)
{
    static int state = 0;

    /* Following "Fig. 210" flowchart on EL70x7 documentation */
    switch (state) {

    /* STARTUP */
    case 0:
        printf("Waiting for 'ready to enable' signal\n");
        ++state;
        break;

    case 1:
        if (txpdo->stm_ready_to_enable == 1)
            ++state;
        break;

    case 2:
        printf("Enabling driver\n");
        rxpdo->stm_enable = 1;
        ++state;
        break;

    case 3:
        if (txpdo->stm_ready == 1)
            ++state;
        break;

    /* START POSITIONING */
    case 4:
        printf("Writing parameters\n");
        rxpdo->pos_velocity = 500;
        rxpdo->pos_acceleration = 20000;
        rxpdo->pos_deceleration = 20000;
        ++state;
        break;

    case 5:
        printf("Homing\n");
        rxpdo->pos_start_type = 0x6000;
        rxpdo->pos_target = 0;
        ++state;
        break;

    case 6:
        printf("Waiting positioning start\n");
        rxpdo->pos_execute = 1;
        ++state;
        break;

    case 7:
        if (txpdo->pos_busy == 1)
            ++state;
        break;

    /* EVALUATE STATUS */
    case 8:
        if (txpdo->pos_busy == 0)
            ++state;
        break;

    case 9:
        printf("'in target' is %s\n", 
               txpdo->pos_in_target == 1 ? "ON" : "OFF");
        ++state;
        break;

    case 10:
        rxpdo->pos_execute = 0;
        if (txpdo->pos_calibrated == 1)
            printf("Axis calibrated\n");
        else
            printf("Axis not calibrated\n");
        ++state;

    case 11:
        if (txpdo->pos_in_target == 0)
            ++state;
        break;

    /* FURTHER POSITIONING */
    case 12:
        rxpdo->pos_start_type = 0x1;
        rxpdo->pos_target = 123467;
        ++state;
        break;

    case 13:
        printf("Waiting positioning start\n");
        rxpdo->pos_execute = 1;
        ++state;
        break;

    case 14:
        if (txpdo->pos_busy == 1)
            ++state;
        break;

    case 15:
        if (txpdo->pos_busy == 0)
            ++state;
        break;

    case 16:
        printf("'in target' is %s\n", 
               txpdo->pos_in_target == 1 ? "ON" : "OFF");
        ++state;
        break;

    case 17:
        rxpdo->pos_execute = 0;
        return 0;
    }

    return 1;
}

int
main(int argc, char *argv[])
{
    /* Initialize the app */
    app_init(&app, "demo");

    /* Configure and initialize the EtherCAT stack */
    if (app_ethercat_init(&app) != ACTION_DONE) {
        die("EtherCAT cannot be initialized");
    }

    app_ethercat_add_slave(&app, 0, 0, machinery_ethercat_slave_ek1100());
    app_ethercat_add_slave(&app, 0, 1, machinery_ethercat_slave_el1809());
    app_ethercat_add_slave(&app, 0, 2, machinery_ethercat_slave_el1809());
    app_ethercat_add_slave(&app, 0, 3, machinery_ethercat_slave_el2809());
    app_ethercat_add_slave(&app, 0, 4, machinery_ethercat_slave_el2809());
    app_ethercat_add_slave(&app, 0, 5, machinery_ethercat_slave_el7047p());
    app_ethercat_add_slave(&app, 0, 6, machinery_ethercat_slave_el7047p());
    //app_ethercat_add_slave(&app, 0, 7, machinery_ethercat_slave_dms4331());

    /* Start the EtherCAT communications */
    app_ethercat_start(&app);

    /* Get the process images */
    rxpdo = app_ethercat_image(&app, 5, 2);
    txpdo = app_ethercat_image(&app, 5, 3);

    /* Initialize the timer: 5 milliseconds per iteration */
    app_timer_init(&app, 5000);

    /* Real-time loop */
    for (;;) {
        /* Read inputs */
        app_ethercat_receive(&app);

        /* Iteration */
        if (iteration() == 0) {
            break;
        }

        /* Write outputs */
        app_ethercat_send(&app);

        /* Wait for next iteration */
        app_timer_wait(&app);
    }

    /* Free the resources allocated for the timer */
    app_timer_free(&app);

    /* Stop the EtherCAT communications */
    app_ethercat_stop(&app);

    /* Free the resources allocated for app */
    app_free(&app);

    return 0;
}
