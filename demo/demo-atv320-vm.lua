-- The following hack is not needed in your own code: it allows to
-- `require 'dsp402'` without having to install `libmachinery`
do
    local arg0 = select(1, ...)
    local dspfile = arg0:gsub('demo/demo%-atv320%-vm.lua$', 'src/dsp402.lua')
    print(dspfile)
    package.preload['dsp402'] = loadfile(dspfile)
end


-- Load the DSP402 script
local dsp402 = require 'dsp402'


-- Instantiate the axis: the demo APIs are provided by the C code
local axis = dsp402.new {
    status_word = demo.status_word,
    control_word = demo.control_word,
    target_point = demo.target_point,
    sdo_download = function (...)
        local done = demo.sdo_download(...)
        if done ~= nil then
            print(string.format('SDO 0x%X <- %d', ...),
                  done and 'done' or 'fail')
        end
        return done
    end,
    step = function (n)
        print(string.format('Step %d', n))
    end,
}


-- Define a simple automata, just to do something:
--
-- 1. initialize the speed
--    (state `start`)
-- 2. increase and set the new speed
--    (state `configure`)
-- 3. rotate forward for a while
--    (state `forward`)
-- 4. increase speed
--    (state `switch`)
-- 5. rotate backward for a while
--    (state `backward`)
-- 6. jump to step 2
local n, speed
local start, configure, forward, switch, backward

function start()
    speed = 50
    return configure
end

function configure()
    n = 0
    speed = speed * 1.5
    return forward
end

function forward()
    n = n + 1
    axis:set_target(speed)
    return n > 600 and switch or forward
end

function switch()
    n = 0
    speed = speed * 1.5
    return backward
end

function backward()
    n = n + 1
    axis:set_target(-speed)
    return n > 600 and configure or backward
end


local loop_function = axis:velocity()
local state = start

-- Return the function called on each iteration
return function ()
    -- The loop function must be called on every iteration
    loop_function()
    -- Run through the automata
    state = state()
end
